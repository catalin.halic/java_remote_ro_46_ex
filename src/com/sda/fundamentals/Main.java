package com.sda.fundamentals;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        afisareZileleSaptamanii();
        afisareLunileAnului();
        introducereNumereSiAfisare();
    }

    public static void afisareZileleSaptamanii() {
        // afisare in consola a zilelor saptamanii
        String[] weekDays = {"Luni", "Marti", "Miercuri", "Joi", "Vineri", "Sambata", "Duminica"};
        for (int i = 0; i < weekDays.length; i++) {
            System.out.println("i = " + i);
            System.out.println(weekDays[i]);
        }
        System.out.println();
    }

    public static void afisareLunileAnului() {
        String[] months = {"Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"};
      //  for (int i = 0; i < months.length; i++){
            for (int i = months.length -1; i >=0; i--){
            System.out.println(months[i]);
        }
        System.out.println();
    }

    public static void introducereNumereSiAfisare() {
        Scanner scanner = new Scanner(System.in); // il folosim pentru a citi de la tastatura
        // citim de la tastatura 5 numere si apoi le afisam

            for(int i = 0; i < 5; i++){
                System.out.println("Introduceti un numar: ");
                int numar= scanner.nextInt();
                System.out.println("Numar introdus de la tastatura:" +numar);
            }
        }
    }
