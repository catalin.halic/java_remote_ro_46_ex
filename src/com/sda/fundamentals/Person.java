package com.sda.fundamentals;

public class Person {
    public static int id;
    private  String name;
    private String phone;

    public Person(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
    //                  personName => parametru, de tipul String, si este local!!!
    public void setName(String personName) {
        // this => face referire la clasa curenta (instanta curenta)
        // la ce contine clasa Person
       this.name = personName;
    }

    public String getName() {
        return name;
    }
}
