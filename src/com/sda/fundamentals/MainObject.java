package com.sda.fundamentals;

public class MainObject {
    public static void main(String[] args) {
        // inline if => if intr-o linie de cod
        int a = 4;
        int b = 5;

        if(a < b) {
            System.out.println("a < b");
            // definita pe plan local in interiorul acestui bloc de cod
            int c = 0;
        } else {
            System.out.println("a >= b");
        }

        System.out.println("----------");
        // ? <=> if
        // ramura de adevar
        // : <=> else
        // ramara de fals
        System.out.println(a < b ? "a < b" : "a >= b");

        System.out.println(2 + 4 + (10 > 0 ? 4 : 5));

        // i => este declarat local, doar intre acolade
        for(int i = 0; i < 10; i++) {
            System.out.println(i);
            System.out.println(a);
        }

        // array de stringuri
        String[] names = {"name1", "name2", "name3"};
//        for(int i =0; i < names.length; i++) {
//            System.out.println(names[i]);
//        }
//                      constructor
        Person p1 = new Person("name1", "phone1");
//        p1.id = 0;
        Person p2 = new Person("name2", "phone2");
//        p2.id = 1;
        Person p3 = new Person("name3", "phone3");
//        p3.id = 2;

        // campurile statice, se apeleaza cu
        // numeClasa.camp/metoda
        Person.id = 100;

        // array de Person
        Person[] persons = {p1, p2, p3};
        for(int i =0; i<persons.length; i++) {
//            persons[i] => returneaza obiectul/elementul de pe pozitia i
            Person p = persons[i];
            System.out.println(p.id + " - " +p.getName());
        }

    }
}
